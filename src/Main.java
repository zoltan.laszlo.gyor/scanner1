import java.util.Scanner;
import java.io.*;
import java.lang.NullPointerException;
import java.lang.StringIndexOutOfBoundsException;

public class Main{
  public static void main(String[] args){
    try{
      File file = new File("input.txt");
      Scanner sc = new Scanner(file);
      int countA = 0;
      int[] array =new int[10];
      while(sc.hasNextLine()){
        String line = sc.nextLine();
        for(int i=0; i<line.length(); i++){
          if(line.charAt(i) == 'a' || line.charAt(i) == 'A'){
            countA++;
          }
        }
      }
      System.out.println("text : " + file);//hogyan kellene ki�ratni a sz�veget? ��+sc, + file, +line
      System.out.println("Number of a in the text: " + countA);
      System.out.println("Numbers in the text: " + array);
    }catch(NullPointerException e){
      System.out.println("null pointer");
    }catch(FileNotFoundException e){
      System.out.println("file nem talalhato");
    }catch(StringIndexOutOfBoundsException e){
      System.out.println("Index is too big");
    }

  }
}
